import { ASTHelper } from './generator_commons';
import { PropertySignature, InterfaceDeclaration, Type } from 'ts-morph';
import * as fs from 'fs';
import { startCase } from 'lodash';
import * as _ from 'lodash';
import { flow, split, dropWhile, join, map, repeat } from 'lodash/fp';


const AstHelper = new ASTHelper();

interface TemplateGenerator {
    readonly filename: string;
    readonly content: (name: string, chNodes: PropertySignature[]) => string;
}


const multiline = (multilineString: string): string => {
  const dropMargins = (from: string[]): string[] => map<string, string>(s => {
    const leftBarIdx = s.indexOf('|');
    return (leftBarIdx >= 0 && leftBarIdx < s.length) ? s.substring(leftBarIdx + 1) : s;
  })(from);

  return flow(
    split('\n'),
    dropMargins,
    join('\n')
  )(multilineString);
}

const ident = (by: number, multilineString: string): string =>
    flow(
      split("\n"),
      map(s => _.repeat(' ', by).concat(s)),
      join('\n')
    )(multilineString);


const formTypeByTsType = (tpe: Type): string => {
  if (tpe.isNumber() || tpe.isNumberLiteral()) return 'number';
  if (tpe.isObject() && tpe.getText() === 'Date') return 'date';
  return 'text';
}

const formattedProp = (property: PropertySignature): string => {
  if (property.getType().isObject() && property.getType().getText() === 'Date')
    return `{extensions.formatDate(entity.${property.getName()})}`;
  return `{entity.${property.getName()}}`;
}


const PugGenerators: TemplateGenerator[] = [
    { filename: 'create.pug',
      content: (name: string, chNodes: PropertySignature[]): string => {

        const formGroups: string = chNodes.map(prop => {
            const pName = prop.getName();
            const readableName = startCase(pName);
            const inpName = pName + 'Input';
            const tpe = formTypeByTsType(prop.getType());
            
            return ident(4, multiline(`.form-group
                                      |  label(for='${inpName}') ${readableName}
                                      |  input#${inpName}.form-control(type='${tpe}' name='${pName}')`
            ));
        }).reduce((l, r) => `${l}\n${r}`);

        return multiline(`extends ../../index.pug
                         |include ../../fielddef.pug
                         |
                         |block submitForm
                         |  form.mt-3(action='/${name}', method='post')
                         |${formGroups}
                         |    .btn-group(role='group' aria-label='Cancel or submit')
                         |      button.btn.btn-secondary(type='button', onclick="window.location = '/customers/list';") Cancel  
                         |      button.btn.btn-primary(type='submit') Create`
                        );
    }}, 
    {
      filename: 'list.pug',
      content: (name: string, chProps: PropertySignature[]): string => {
        const href = (action: 'update' | 'delete'): string => "`/" + name + "/" + action + "/${entity.id}`";
        const createHref = "`/" + name + "/create`";
        const tdNodesAsStr: string = chProps
            .map(node => ident(4, `td #${formattedProp(node)}`))
            .reduce((l, r) => `${l}\n${r}`);

        const thNamesAsStr: string = chProps
            .map(node => ident(8, `th(scope='col') ${startCase(node.getName())}`))
            .reduce((l, r) => `${l}\n${r}`);

        return multiline(`extends ../../index.pug
                         |mixin entityTr(entity)
                         |  tr
                         |    th(scope='row') #{entity.id}
                         |${tdNodesAsStr}
                         |    td 
                         |      a.btn-sm.btn-secondary(href=${href('update')}) Update
                         |    td
                         |      a.btn-sm.btn-danger(href=${href('delete')}) Delete
                         |
                         |block submitForm
                         |  a.mt-5.btn.btn-primary(href=${createHref}) Add new
                         |
                         |block entityTable
                         |  table.table
                         |    thead
                         |      tr
                         |        th(scope='col') ID
                         |${thNamesAsStr}
                         |        th(scope='col') 
                         |        th(scope='col') 
                         |    tbody
                         |      for entity in entities
                         |        +entityTr(entity)`);
    }},
    {
      filename: 'update.pug',
      content: (name: string, chProps: PropertySignature[]): string => {
        const action = "`/" + name + "/${entity.id}`";
        const formGroups: string = chProps.map(prop => {
            const pName = prop.getName();
            const readableName = startCase(pName);
            const inpName = pName + 'Input';
            const placeholderText = "`$" + formattedProp(prop) + "`";
            const tpe = formTypeByTsType(prop.getType());
            
            return ident(4, multiline(`.form-group
                                      |  label(for='${inpName}') ${readableName}
                                      |  input#${inpName}.form-control(type='${tpe}' name='${pName}' value=${placeholderText})`
            ));
        }).reduce((l, r) => `${l}\n${r}`);
        const entityIdPlaceholder = "`${entity.id}`";

        return multiline(`extends ../../index.pug
                         |include ../../fielddef.pug
                         |
                         |block submitForm
                         |  form.mt-3(action=${action}, method='post')
                         |    .form-group
                         |      label(for='idInput') ID
                         |      input#idInput.form-control(type='text' name='id' placeholder=${entityIdPlaceholder} readonly)
                         |${formGroups}
                         |    .btn-group(role='group' aria-label='Cancel or submit')
                         |      button.btn.btn-secondary(type='button', onclick="window.location = '/${name}/list';") Cancel  
                         |      button.btn.btn-primary(type='submit') Update`
                         );
    }}
];


const generateViews = (iEntity: InterfaceDeclaration): void => {

  console.info(`Creating views for ${iEntity.getName()} ...`);

  const name = iEntity
        .getName()
        .substr(1)  // drop 'I' prefix
        .toLowerCase() + 's';

    const nodes: PropertySignature[] = iEntity.getProperties();

    PugGenerators.forEach(generator => {
        console.info(`Writing ${name}/${generator.filename}...`);

        const viewsPath = `./static/views/entities/${name}/`;

        fs.mkdirSync(viewsPath, { recursive: true });
        fs.writeFileSync(
            viewsPath + generator.filename,
            generator.content(name, nodes),
            'utf-8'
        );
    });
};

console.info(`Compiler instantiated, generating views for ${AstHelper.IWithId.getName()} descendants...`)
AstHelper.traverse(generateViews);
