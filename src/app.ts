import * as express from 'express';
import { Application } from 'express';
import { join } from 'path';
import { CrudController } from './controller/crud_controller';
import { AddressInfo } from 'net';
import { urlencoded } from 'body-parser';


export default class App {
    private readonly expressApp: Application;
    public readonly port: number

    constructor(port: number,
                controllers: CrudController<any>[]) {
        this.port = port;
        this.expressApp = express();

        this.expressApp.set('views', join(__dirname, '/static/views'));
        this.expressApp.set('view engine', 'pug');

        this.expressApp.use(urlencoded({extended: true}));

        controllers.forEach( controller => controller.bindToApp(this.expressApp) );
    }


    serveForever() {
        const listener = this.expressApp.listen(this.port).address() as AddressInfo;
        console.log(
            `Express app started on ${listener.address}:${listener.port}`
        );
    }
};