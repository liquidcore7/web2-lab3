import App from './app';
import { CrudController } from './controller/crud_controller';
import { MongoRepo } from './repo/mongo_repo';
import { ICustomer, Customer } from './domain/customer';
import { CustomersCollectionModel } from './repo/models/customer';
import { IExecutor, Executor } from './domain/executor';
import { ExecutorsCollectionModel } from './repo/models/executor';
import { IProject, Project } from './domain/project';
import { ProjectsCollectionModel } from './repo/models/project';
import { IExecution, Execution } from './domain/execution';
import { ExecutionsCollectionModel } from './repo/models/execution';
import { connect } from 'mongoose';


const application = new App(
    Number(process.env.PORT).valueOf() || 3000,
    [
        new CrudController('/customers', new MongoRepo<ICustomer>(CustomersCollectionModel, [
            Customer('Andrii', 20_000),
            Customer('NU LP', 999_999),
            Customer('LNMU medical university', 1_550_000)
        ])),
        new CrudController('/executors', new MongoRepo<IExecutor>(ExecutorsCollectionModel, [
            Executor('SoftServe Inc.', 20, 1300),
            Executor('EPAM', 25, 3000),
            Executor('JetSoftPro', 5, 100)
        ])),
        new CrudController('/projects', new MongoRepo<IProject>(ProjectsCollectionModel, [
            Project('Healthcare platform', 2),
            Project('Virtual educational center', 1, 'A scalable platform for MOOCs')
        ])),
        new CrudController('/executions', new MongoRepo<IExecution>(ExecutionsCollectionModel, [
            Execution(0, 1, new Date('2014-01-04'), new Date('2020-01-01')),
            Execution(1, 0, new Date('2018-05-11'))
        ]))
    ]
);

connect('mongodb://127.0.0.1:27017', { useNewUrlParser: true, user: 'root', pass: 'rootpassword', useUnifiedTopology: true })
    .then(_ => application.serveForever());