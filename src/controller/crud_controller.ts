import { ICrudRepo } from "../repo/crud_repo";
import { Entity, IWithID } from "../domain/entity";
import { Request, Response, Application } from "express";
import { ParamsDictionary } from 'express-serve-static-core';


export class CrudController<I extends IWithID<any>> {
    private readonly route: string;
    private readonly pugRoute: string;

    private readonly crudRepo: ICrudRepo<any, I>;

    constructor(
        route: string, 
        crudRepo: ICrudRepo<any, I>,
        pugRoute?: string) {
            this.route = route;
            this.pugRoute = pugRoute || ('entities' + route);
            this.crudRepo = crudRepo; 
    }

    private redirectToList = (req: Request, res: Response): Promise<void> =>
        Promise.resolve(res.redirect(req.baseUrl + this.route + '/list'));
    

    getCreateForm = (req: Request, res: Response): Promise<void> =>
        Promise.resolve(res.render(this.pugRoute + '/create'));
    

    getUpdateForm = (req: Request<IIdParams>, res: Response): Promise<void> =>
        this.crudRepo
            .findById(req.params.id)
            .then(entity => new EntityResponse(entity as Entity<any, I>))
            .then(responseBody => res.render(this.pugRoute + '/update', responseBody));


    create = (req: Request, res: Response): Promise<void> =>
        this.crudRepo
            .create( req.body as I )
            .then(_ => this.redirectToList(req, res));


    list = (req: Request, res: Response): Promise<void> => 
        this.crudRepo
            .fetchAll()
            .then(all => new ListResponse(all))
            .then(responseBody => res.render(this.pugRoute + '/list', responseBody));
        

    delete = (req: Request<IIdParams>, res: Response): Promise<void> =>
        this.crudRepo
            .deleteById(req.params.id)
            .then(_ => this.redirectToList(req, res));


    update = (req: Request<IIdParams>, res: Response): Promise<void> =>
        this.crudRepo
            .update(req.params.id, req.body as I)
            .then(_ => this.redirectToList(req, res));


    bindToApp = (app: Application): Application =>
        app.get(this.route + '/list', this.list)
            .get(this.route + '/create', this.getCreateForm)
            .get(this.route + '/update/:id', this.getUpdateForm)
            .post(this.route, this.create)
            .post(this.route + '/:id', this.update)
            .get(this.route + '/delete/:id', this.delete);
};


interface IIdParams extends ParamsDictionary {
    id: string; 
};

const Extensions = {
    formatDate: (date: Date): string => {
        const month = date.getMonth() + 1;
        const day = date.getDate() + 1;

        return `${date.getFullYear()}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;
    }
}

class ListResponse<Id, I extends IWithID<Id>> {
    public readonly entities: I[];
    public readonly extensions = Extensions;

    constructor(entities: Entity<Id, I>[]) {
        this.entities = entities.map(e => e.body);
    }
};

class EntityResponse<Id, I extends IWithID<Id>> {
    public readonly entity: I;
    public readonly extensions = Extensions;

    constructor(entity: Entity<Id, I>) {
        this.entity = entity.body;
    }
}
