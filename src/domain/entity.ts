export interface IWithID<Id> {
  id?: Id;
};

export class Entity<Id, T extends IWithID<Id>> {
  public readonly body: T;

  constructor(body: T) {
    this.body = body;
  }

  withId(newId: Id): Entity<Id, T> {
    this.body.id = newId;
    return this;
  }
};