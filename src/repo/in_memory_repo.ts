import { Entity, IWithID } from '../domain/entity';
import * as _ from 'lodash';
import { ICrudRepo } from './crud_repo';

export class InMemoryRepo<I extends IWithID<number>> implements ICrudRepo<number, I> {
  private readonly elements: Array<Entity<number, I> | null>;
  private readonly droppedIds: number[] = [];

  constructor(of?: I[]) {
    this.elements = _.map(
      { ...of } || {},
      (value: I, key: string) => new Entity<number, I>(value).withId(Number(key).valueOf())
    );
  }

  findById(id: number): Promise<Entity<number, I> | null> {
    return Promise.resolve(this.elements.length > id ? this.elements[id] : null);
  }

  create(elem: I): Promise<Entity<number, I>> {
    const generatedId: number = this.droppedIds.pop() || this.elements.length;
    const entity = new Entity<number, I>(elem);
    const entityWithId = entity.withId(generatedId);

    if (generatedId < this.elements.length) {
      this.elements[generatedId] = entityWithId;
    } else this.elements.push(entityWithId);

    return Promise.resolve(entityWithId);
  }

  deleteById(id: number): Promise<void> {
    this.droppedIds.push(id);
    this.elements[id] = null;

    return Promise.resolve();
  }

  update(oldId: number, newElem: I): Promise<Entity<number, I> | null> {
    return this.findById(oldId)
        .then(oldElem => {
          if (oldElem != null) {
            const newEntity = new Entity<number, I>(newElem);
            const withCorrectId = newEntity.withId(oldId);
            this.elements[oldId] = withCorrectId;

            return withCorrectId;
          }
          return oldElem;
        });
  }

  fetchAll(): Promise<Entity<number, I>[]> {
    return Promise.resolve(
      this.elements
        .filter(entity => entity != null)
        .map(entity => entity as Entity<number, I>)
    );
  }
}
