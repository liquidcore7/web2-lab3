import { Entity, IWithID } from "../domain/entity";
import { ICrudRepo } from './crud_repo';
import { Document, Model } from "mongoose";


export class MongoRepo<I extends IWithID<any>> implements ICrudRepo<any, I> {

    private readonly modelTC: Model<I & Document>;

    constructor(modelTC: Model<I & Document>, initialData?: I[]) {
        this.modelTC = modelTC;

        if (initialData != undefined) { // TODO: wait for promise in constructor?
            modelTC
                .deleteMany({})
                .exec()
                .then(_ => Promise.all(
                    initialData.map(entity => this.create(entity))
                ));
        }
    }

    findById(id: any): Promise<Entity<any, I> | null> {
        return this.modelTC
            .findById(id)
            .map(doc => doc == null ? null : new Entity(doc))
            .exec();
    }

    create(elem: I): Promise<Entity<any, I>> {
        return this.modelTC
            .create(elem)
            .then(doc => new Entity(doc));
    }

    deleteById(id: any): Promise<void> {
        return this.modelTC
        .deleteOne({ _id: id })
        .map(result => {})
        .exec();
    }

    update(oldId: any, newElem: I): Promise<Entity<any, I> | null> {
        return this.modelTC
            .updateOne({_id: oldId}, newElem)
            .map(res => {
                const updated = res as I;
                return updated == null ? null : new Entity(updated)
            })
            .exec();
    }

    fetchAll(): Promise<Entity<any, I>[]> {
        return this.modelTC.find()
            .map(docs => docs.map(doc => new Entity(doc)))
            .exec();
    }

}