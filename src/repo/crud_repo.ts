import { Entity, IWithID } from '../domain/entity';


export interface ICrudRepo<Id, I extends IWithID<Id>> {
  findById(id: Id): Promise<Entity<Id, I> | null>;

  create(elem: I): Promise<Entity<Id, I>>;

  deleteById(id: Id): Promise<void>;

  update(oldId: Id, newElem: I): Promise<Entity<Id, I> | null>;

  fetchAll(): Promise<Entity<Id, I>[]>;
}
